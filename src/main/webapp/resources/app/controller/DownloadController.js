var app = angular.module( 'downloader', [] );

app.controller( 'DownloadController', ['$scope', '$http', '$interval', '$rootScope', '$timeout', function( $scope, $http, $interval, $rootScope, $timeout) {
	/**
	 * Object that will be used to send content to server.
	 */
	$scope.config = {
			url : null,
			id : null
	};
	
	/**
	 * Menus available for actions.
	 */
	$scope.menus = [
		{ text : 'PlayList', id : 0 },
		{ text : 'Video', id : 1 }
	];
	
	$scope.active = 0;
	
	/**
	 * Is current menu active.
	 */
	$scope.isActive = function( index ) {
		return index == $scope.active;
	};
	
	/**
	 * Make this menu active.
	 */
	$scope.setActive = function( index ) {
		$scope.active = index;
	}
	
	/**
	 * Is playlist download active.
	 */
	$scope.isPlaylistDownloadActive = function() {
		return $scope.active == 0;
	};
	
	/**
	 * Start Downloading.
	 */
	$scope.download = function( result ) {
		if( angular.isDefined( result )) {
			$scope.config.url = 'http://www.youtube.com/watch?v=' + result.snippet.resourceId.videoId;
			
			$http.post( 'api/download', $scope.config )
			$scope.tasks.id = result.snippet.resourceId.videoId;
			$scope.tasks.url  = $scope.config.url;
			
			$rootScope.$broadcast( 'StartMonitoring' );
		}
	}
	
	/**
	 * Query google apis to retrieve playlist info.
	 */
	$scope.query = function() {
		
		var url = 'https://www.googleapis.com/youtube/v3/playlistItems?' + 
							        'part=snippet&' + 
							        'playlistId=' + $scope.config.id + '&' + 
							        'key=' + 'AIzaSyCI8n3-9sYXespfHDIqy0ve42eR2_17REw' + '&' + 
							        'maxResults=50';
		
		$http.get( url )
				.success( function( data ) {
					$scope.results = data.items;
				})
	}
	
	$scope.tasks = {
			repeat : null,
			id : null
	};
	
	$scope.queue = {
			at : null
	};
	
	$scope.getStatus = function() {
		$http.post( 'api/status', { url : $scope.tasks.url })
			.success( function( data ) {
				var progress = data.data;
				
				if( progress == 100 ) {
					$rootScope.$broadcast( 'StopMonitoring' );
					$timeout( function() {$scope.moveQueue();}, 1000 );
				}
				
				angular.forEach( $scope.results, function( result ) {
					if( result.snippet.resourceId.videoId == $scope.tasks.id ) {
						result.progress = progress;
					}
				})
			})
	};
	
	$scope.$on( 'StartMonitoring', function() {
		$scope.tasks.repeat = $interval( function() {
			$scope.getStatus();
		}, 5000 );
	});
	
	$scope.$on( 'StopMonitoring', function() {
		$interval.cancel( $scope.tasks.repeat );
	});
	
	$scope.getDownloadProgress = function( result ) {
		if( result.progress != null && angular.isDefined( result.progress )) {
			return result.progress + '%';
		}
		
		return '0%';
	};
	
	$scope.processQueue = function() {
		
		var result = $scope.results[ $scope.queue.at ];
		
		$scope.download( result );
	};
	
	$scope.moveQueue = function() {
		
		if( $scope.queue.at == null ) {
			return;
		}
		
		$scope.queue.at++;
		
		if( $scope.queue.at == $scope.results.length ) {
			$scope.queue.at = null;
			return;
		}
		
		$scope.processQueue();
	};
	
	$scope.downloadAll = function() {
		$scope.queue.at = -1;
		$scope.moveQueue();
	}
}]);