package com.paras.download.youtube.controller;

import java.io.File;
import java.net.MalformedURLException;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.paras.download.youtube.downloader.VideoDownloader;
import com.paras.download.youtube.dto.DownloadRequest;
import com.paras.download.youtube.status.StatusHolder;
import com.paras.framework.web.base.Response;

/**
 * This class is responsible for handling video query related operations.
 * @author Gaurav Tiwari
 *
 */
@Controller
public class QueryController {

	private static final Logger LOGGER = Logger.getLogger( QueryController.class );
	
	@Autowired
	private VideoDownloader downloader;
	
	@RequestMapping( value = "query", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody Response query( @RequestBody DownloadRequest request, HttpServletResponse httpResponse ) {
		LOGGER.info( "In QueryController | Starting Execution of query ");
		LOGGER.info( "In QueryController | Finished Execution of query ");
		
		return null;
	}
	
	@RequestMapping( value = "download", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody void download( @RequestBody DownloadRequest request, HttpServletResponse httpResponse ) throws MalformedURLException {
		LOGGER.info( "In QueryController | Starting Execution of download ");
		
		downloader.doDownload( request.getUrl(), new File( "G:\\Video" ));
		
		LOGGER.info( "In QueryController | Finished Execution of download ");
	}
	
	@RequestMapping( value = "status", method = RequestMethod.POST, produces = "application/json" )
	public @ResponseBody Response getStatus( @RequestBody DownloadRequest request, HttpServletResponse httpResponse ) {
		LOGGER.info( "In QueryController | Starting Execution of getStatus ");
		
		String progress = StatusHolder.get( request.getUrl());
		
		if( "100".equals(progress)) {
			StatusHolder.remove( request.getUrl() );
		}
		Response response = new Response( "Y", null, progress );
		
		LOGGER.info( "In QueryController | Finished Execution of getStatus ");
		
		return response;
	}
}
