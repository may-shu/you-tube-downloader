package com.paras.download.youtube.status;

import java.util.HashMap;
import java.util.Map;

/**
 * This class will simply hold progress status of each download.
 * @author Gaurav Tiwari
 *
 */
public class StatusHolder {

	private static Map<String, String> progressMap = new HashMap<String, String>();
	
	public static void store( String id, String progress ) {
		progressMap.put(id,  progress );
	}
	
	public static String get( String id ) {
		return progressMap.get( id );
	}
	
	public static void remove( String id ) {
		progressMap.remove(id);
	}
	
}
