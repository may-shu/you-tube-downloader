package com.paras.download.youtube.downloader;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.atomic.AtomicBoolean;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.github.axet.vget.VGet;
import com.github.axet.vget.info.VGetParser;
import com.github.axet.vget.info.VideoInfo;
import com.paras.download.youtube.status.StatusHolder;

/**
 * Class responsible to download video.
 * @author Gaurav Tiwari
 *
 */
@Service
public class VideoDownloader {
	
	@Async
	public void doDownload( String urlToDownload, File path ) throws MalformedURLException {
		URL url = new URL( urlToDownload );
		final AtomicBoolean stop = new AtomicBoolean(false);
		
		VGetParser parser = VGet.parser( url );
		VideoInfo videoInfo = parser.info(url);
		
		Downloader get = new Downloader( videoInfo, path );
		DownloadStatus monitor = new DownloadStatus( videoInfo );
		
		get.download( parser, stop, monitor );
		StatusHolder.store( urlToDownload.toString(), "100");
	}
	
}
