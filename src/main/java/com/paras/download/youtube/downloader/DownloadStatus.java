package com.paras.download.youtube.downloader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.github.axet.vget.info.VideoFileInfo;
import com.github.axet.vget.info.VideoInfo;
import com.github.axet.vget.info.VideoInfo.States;
import com.github.axet.wget.SpeedInfo;
import com.github.axet.wget.info.DownloadInfo;
import com.github.axet.wget.info.DownloadInfo.Part;
import com.paras.download.youtube.status.StatusHolder;

/**
 * Class that will store and notify details about video download.
 * @author Gaurav Tiwari
 *
 */
public class DownloadStatus implements Runnable {

	/**
	 * Video Info of the video.
	 */
	private VideoInfo info;
	
	private long last;
	
	private Map<VideoFileInfo, SpeedInfo> map = new HashMap<VideoFileInfo, SpeedInfo>();
	private static final Logger LOGGER = Logger.getLogger( DownloadStatus.class );
	
	public DownloadStatus( VideoInfo info ) {
		this.info = info;
	}
	
	public SpeedInfo getSpeedInfo( VideoFileInfo videoFileInfo ) {
		SpeedInfo speedInfo = map.get( videoFileInfo );
		
		if( speedInfo == null ) {
			speedInfo = new SpeedInfo();
			speedInfo.start( videoFileInfo.getCount() );
			map.put(videoFileInfo, speedInfo);
		}
		
		return speedInfo;
	}
	
	@SuppressWarnings("unused")
	@Override
    public void run() {
		VideoInfo videoinfo = info;
        List<VideoFileInfo> dinfoList = videoinfo.getInfo();

        // notify app or save download state
        // you can extract information from DownloadInfo info;
        switch (videoinfo.getState()) {

        case ERROR:
            System.out.println(videoinfo.getState() + " " + videoinfo.getDelay());

            if (dinfoList != null) {
                for (DownloadInfo dinfo : dinfoList) {
                    System.out.println("file:" + dinfoList.indexOf(dinfo) + " - " + dinfo.getException() + " delay:"
                            + dinfo.getDelay());
                }
            }
            break;
        case RETRYING:
            System.out.println(videoinfo.getState() + " " + videoinfo.getDelay());

            if (dinfoList != null) {
                for (DownloadInfo dinfo : dinfoList) {
                    System.out.println("file:" + dinfoList.indexOf(dinfo) + " - " + dinfo.getState() + " "
                            + dinfo.getException() + " delay:" + dinfo.getDelay());
                }
            }
            break;
        case DOWNLOADING:
            long now = System.currentTimeMillis();
            if (now - 1000 > last) {
                last = now;

                String parts = "";

                for (VideoFileInfo dinfo : dinfoList) {
                    SpeedInfo speedInfo = getSpeedInfo(dinfo);
                    speedInfo.step(dinfo.getCount());

                    List<Part> pp = dinfo.getParts();
                    if (pp != null) {
                        // multipart download
                        for (Part p : pp) {
                            if (p.getState().equals(States.DOWNLOADING)) {
                                parts += String.format("part#%d(%.2f) ", p.getNumber(),
                                        p.getCount() / (float) p.getLength());
                            }
                        }
                    }
                    
                    String progress = String.valueOf(( (dinfo.getCount() / (float) dinfo.getLength()) * 100 ));                    
                    StatusHolder.store( info.getWeb().toString(), progress);
                    
                    LOGGER.info( "Downloading : " + info.getTitle() + " at " + progress );
                }
            }
            break;
        default:
            break;
        }
    }
	
	public static String formatSpeed(long s) {
        if (s > 0.1 * 1024 * 1024 * 1024) {
            float f = s / 1024f / 1024f / 1024f;
            return String.format("%.1f GB/s", f);
        } else if (s > 0.1 * 1024 * 1024) {
            float f = s / 1024f / 1024f;
            return String.format("%.1f MB/s", f);
        } else {
            float f = s / 1024f;
            return String.format("%.1f kb/s", f);
        }
    }
}
