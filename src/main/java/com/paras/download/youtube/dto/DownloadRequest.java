package com.paras.download.youtube.dto;

/**
 * A class that represents a request to download a video.
 * @author Gaurav Tiwari
 *
 */
public class DownloadRequest {

	private String url;
	private String id;
	
	public String getUrl() {
		return url;
	}
	
	public void setUrl( String url ) {
		this.url = url;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
